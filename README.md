This is not a real project.
This repository is a stand-in for testing purposes.
This repository should never be used except in dry-run testing.
If a link *did* direct you here, then something has gone badly wrong.
Please feel free to either ignore the request or report the bug at https://gitlab.com/automated-browser-interaction/automatically-renew-gitlab/-/issues.
